import { CliAngular4EducPage } from './app.po';

describe('cli-angular4-educ App', () => {
  let page: CliAngular4EducPage;

  beforeEach(() => {
    page = new CliAngular4EducPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
